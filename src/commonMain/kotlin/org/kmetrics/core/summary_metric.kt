package org.kmetrics.core

/**
 * A metric that contains a value, and *may* also combine some metrics.
 * @param T The type of value that the metric will hold. This type is also used for [metrics].
 */
class SummaryMetric<T : Number>(override val name: String) : Metric<T> {
    override var desc: String = ""
    override lateinit var value: T
    /** Metrics that the [summary][SummaryMetric] *may* combine. */
    val metrics: MutableList<Metric<T>> = mutableListOf()
    override val labels: MutableList<Label> = mutableListOf()

    override fun createText(): String = buildString {
        append("# TYPE $name summary\n")
        if (desc.isNotEmpty()) append("# HELP $desc\n")
        if (labels.isEmpty()) append("$name $value") else append("$name{${createLabelsText()}} $value")
        metrics.forEach { m ->
            append("\n")
            append(createMetricText(m))
        }
        append("\n")
    }

    private fun createMetricText(metric: Metric<T>) = "${name}_${metric.name} ${metric.value}"
}

/**
 * Creates a new Summary metric.
 * @param name The metric name.
 * @param init Initialisation block for the metric.
 * @param T The type of value that the metric will hold.
 * @return A new Summary metric.
 */
fun <T : Number> createSummaryMetric(name: String, init: SummaryMetric<T>.() -> Unit): SummaryMetric<T> {
    val result = SummaryMetric<T>(name)
    init(result)
    return result
}
