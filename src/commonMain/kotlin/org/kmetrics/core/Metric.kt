package org.kmetrics.core

/**
 * Base for metric types.
 * @param T The type of value that the metric will hold.
 */
interface Metric<T : Number> {
    val name: String
    var desc: String
    var value: T
    /** Extra metadata that is associated with the metric. */
    val labels: MutableList<Label>

    /**
     * Creates a textual representation of the metric using the
     * [Prometheus Text format](https://github.com/prometheus/docs/blob/master/content/docs/instrumenting/exposition_formats.md).
     */
    fun createText(): String

    /** Creates a textual representation of the metric's [labels]. */
    fun createLabelsText(): String = buildString {
        labels.forEach { (k, v) -> append("$k=\"$v\",") }
        removeSuffix(",")
    }
}
