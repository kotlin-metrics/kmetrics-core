package org.kmetrics.core

/**
 * A metric representing a counter that goes up and down incrementally.
 * @param T The type of value that the metric will hold.
 */
class GaugeMetric<T : Number>(override val name: String) : Metric<T> {
    override var desc: String = ""
    override lateinit var value: T
    override val labels: MutableList<Label> = mutableListOf()

    override fun createText(): String = buildString {
        append("# TYPE $name gauge\n")
        if (desc.isNotEmpty()) append("# HELP $desc\n")
        if (labels.isEmpty()) append("$name $value") else append("$name{${createLabelsText()}} $value")
        append("\n")
    }
}

/**
 * Creates a new Gauge metric.
 * @param name The metric name.
 * @param init Initialisation block for the metric.
 * @param T The type of value that the metric will hold.
 * @return A new Gauge metric.
 */
fun <T : Number> createGaugeMetric(name: String, init: GaugeMetric<T>.() -> Unit): GaugeMetric<T> {
    val result = GaugeMetric<T>(name)
    init(result)
    return result
}
