package org.kmetrics.core

/**
 * A metric representing a histogram that contains a value, and *may* also contain extra values.
 * @param T The type of value/extra values that the metric will hold.
 */
class HistogramMetric<T : Number>(override val name: String) : Metric<T> {
    override var desc: String = ""
    override lateinit var value: T
    /** Extra values that the [histogram][HistogramMetric] *may* contain. */
    val extraValues: MutableList<T> = mutableListOf()
    override val labels: MutableList<Label> = mutableListOf()

    override fun createText(): String = buildString {
        append("# TYPE $name histogram\n")
        if (desc.isNotEmpty()) append("# HELP $desc\n")
        if (labels.isEmpty()) append("$name $value") else append("$name{${createLabelsText()}} $value")
        extraValues.forEach { ev ->
            append("\n")
            if (labels.isEmpty()) append("$name $ev") else append("$name{${createLabelsText()}} $ev")
        }
        append("\n")
    }
}

/**
 * Creates a new Histogram metric.
 * @param name The metric name.
 * @param init Initialisation block for the metric.
 * @param T The type of value that the metric will hold.
 * @return A new Histogram metric.
 */
fun <T : Number> createHistogramMetric(name: String, init: HistogramMetric<T>.() -> Unit): HistogramMetric<T> {
    val result = HistogramMetric<T>(name)
    init(result)
    return result
}
