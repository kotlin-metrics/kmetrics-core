package org.kmetrics.core

/**
 * Metadata for a metric.
 * @param name The name of the metadata.
 * @param value Metadata value.
 */
data class Label(val name: String, var value: String)
