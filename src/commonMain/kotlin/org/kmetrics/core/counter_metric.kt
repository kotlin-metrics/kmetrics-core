package org.kmetrics.core

/**
 * A metric representing a counter that goes up and down incrementally.
 * @param T The type of value that the metric will hold.
 */
class CounterMetric<T : Number>(override val name: String) : Metric<T> {
    override var desc: String = ""
    override lateinit var value: T
    override val labels: MutableList<Label> = mutableListOf()

    override fun createText(): String = buildString {
        append("# TYPE $name counter\n")
        if (desc.isNotEmpty()) append("# HELP $desc\n")
        if (labels.isEmpty()) append("$name $value") else append("$name{${createLabelsText()}} $value")
        append("\n")
    }
}

/**
 * Creates a new Counter metric.
 * @param name The metric name.
 * @param init Initialisation block for the metric.
 * @param T The type of value that the metric will hold.
 * @return A new Counter metric.
 */
fun <T : Number> createCounterMetric(name: String, init: CounterMetric<T>.() -> Unit): CounterMetric<T> {
    val result = CounterMetric<T>(name)
    init(result)
    return result
}
