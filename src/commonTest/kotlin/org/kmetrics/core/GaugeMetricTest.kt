package org.kmetrics.core

import kotlin.test.Test

expect class GaugeMetricTest : MetricTestBase {
    @Test
    override fun `create text with name, and value`()

    @Test
    override fun `create text with name, desc, and value`()
}
