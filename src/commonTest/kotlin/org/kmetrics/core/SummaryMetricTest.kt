package org.kmetrics.core

import kotlin.test.Test

expect class SummaryMetricTest : MetricTestBase {
    @Test
    override fun `create text with name, and value`()

    @Test
    override fun `create text with name, desc, and value`()

    @Test
    fun `create text with name, value, and metrics`()
}
