package org.kmetrics.core

import kotlin.test.Test
import kotlin.test.assertEquals

actual class HistogramMetricTest : MetricTestBase {
    @Test
    actual override fun `create text with name, and value`() {
        val actual = createHistogramMetric<Int>("lucky_number") { value = 7 }.createText()
        val expected =
            """
            # TYPE lucky_number histogram
            lucky_number 7${"\n"}
            """.trimIndent()
        println("Actual:\n$actual")
        assertEquals(expected = expected, actual = actual)
    }

    @Test
    actual override fun `create text with name, desc, and value`() {
        val actual = createHistogramMetric<Int>("lucky_number") {
            value = 7
            desc = "Lucky Number"
        }.createText()
        val expected =
            """
            # TYPE lucky_number histogram
            # HELP Lucky Number
            lucky_number 7${"\n"}
            """.trimIndent()
        println("Actual:\n$actual")
        assertEquals(expected = expected, actual = actual)
    }

    @Test
    actual fun `create text with name, desc, value, and extra values`() {
        val actual = createHistogramMetric<Int>("lucky_number") {
            value = 7
            desc = "Lucky Number"
            extraValues += 8
            extraValues += 2
        }.createText()
        val expected =
            """
            # TYPE lucky_number histogram
            # HELP Lucky Number
            lucky_number 7
            lucky_number 8
            lucky_number 2${"\n"}
            """.trimIndent()
        println("Actual:\n$actual")
        assertEquals(expected = expected, actual = actual)
    }
}
