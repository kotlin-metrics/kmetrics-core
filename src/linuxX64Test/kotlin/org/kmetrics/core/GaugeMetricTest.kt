package org.kmetrics.core

import kotlin.test.Test
import kotlin.test.assertEquals

actual class GaugeMetricTest : MetricTestBase {
    @Test
    actual override fun `create text with name, and value`() {
        val actual = createGaugeMetric<Int>("lucky_number") { value = 7 }.createText()
        val expected =
            """
            # TYPE lucky_number gauge
            lucky_number 7${"\n"}
            """.trimIndent()
        println("Actual:\n$actual")
        assertEquals(expected = expected, actual = actual)
    }

    @Test
    actual override fun `create text with name, desc, and value`() {
        val actual = createGaugeMetric<Int>("lucky_number") {
            value = 7
            desc = "Lucky Number"
        }.createText()
        val expected =
            """
            # TYPE lucky_number gauge
            # HELP Lucky Number
            lucky_number 7${"\n"}
            """.trimIndent()
        println("Actual:\n$actual")
        assertEquals(expected = expected, actual = actual)
    }
}
