package org.kmetrics.core

import kotlin.test.Test
import kotlin.test.assertEquals

actual class SummaryMetricTest : MetricTestBase {
    @Test
    actual override fun `create text with name, and value`() {
        val actual = createSummaryMetric<Int>("lucky_number") { value = 7 }.createText()
        val expected =
            """
            # TYPE lucky_number summary
            lucky_number 7${"\n"}
            """.trimIndent()
        println("Actual:\n$actual")
        assertEquals(expected = expected, actual = actual)
    }

    @Test
    actual override fun `create text with name, desc, and value`() {
        val actual = createSummaryMetric<Int>("lucky_number") {
            value = 7
            desc = "Lucky Number"
        }.createText()
        val expected =
            """
            # TYPE lucky_number summary
            # HELP Lucky Number
            lucky_number 7${"\n"}
            """.trimIndent()
        println("Actual:\n$actual")
        assertEquals(expected = expected, actual = actual)
    }

    @Test
    actual fun `create text with name, value, and metrics`() {
        val baseName = "prometheus_rule_evaluation_duration_seconds"
        val sumMetric = createGaugeMetric<Double>("sum") { value = 1.62 }
        val countMetric = createCounterMetric<Double>("count") { value = 1.11 }
        val actual = createSummaryMetric<Double>(baseName) {
            value = 6.4
            metrics += sumMetric
            metrics += countMetric
        }.createText()
        val expected =
            """
            # TYPE $baseName summary
            $baseName 6.4
            ${baseName}_sum 1.62
            ${baseName}_count 1.11${"\n"}
            """.trimIndent()
        println("Actual:\n$actual")
        assertEquals(expected = expected, actual = actual)
    }
}
