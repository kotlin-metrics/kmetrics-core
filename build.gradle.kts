import java.util.*

val gitLabSettings = fetchGitLabSettings()
val projectSettings = fetchProjectSettings()

group = "org.kmetrics"
version = if (projectSettings.isDevVer) "${projectSettings.libVer}-dev" else projectSettings.libVer

plugins {
    kotlin("multiplatform") version "1.6.20"
    `maven-publish`
}

repositories {
    mavenCentral()
}

kotlin {
    linuxX64()

    sourceSets {
        val kotlinVer = "1.6.20"
        getByName("commonMain") {
            dependencies {
                implementation(kotlin("stdlib-common", kotlinVer))
            }
        }
        getByName("commonTest") {
            dependencies {
                implementation(kotlin("stdlib-common", kotlinVer))
                implementation("org.jetbrains.kotlin:kotlin-test-common:$kotlinVer")
                implementation("org.jetbrains.kotlin:kotlin-test-annotations-common:$kotlinVer")
            }
        }
    }
}

publishing {
    repositories {
        if (gitLabSettings.publishingEnabled) {
            maven {
                val projectId = gitLabSettings.projectId
                url = uri("https://gitlab.com/api/v4/projects/$projectId/packages/maven")
                credentials(HttpHeaderCredentials::class.java) {
                    name = "Private-Token"
                    value = gitLabSettings.token
                }
                authentication {
                    create("header", HttpHeaderAuthentication::class.java)
                }
            }
        }
    }
}

tasks.getByName("publish") {
    doFirst { println("Project Version: ${project.version}") }
}

data class GitLabSettings(val token: String, val projectId: Int, val publishingEnabled: Boolean)

fun fetchGitLabSettings(): GitLabSettings {
    var token = ""
    var projectId = -1
    val properties = Properties()
    var publishingEnabled = true
    file("gitlab.properties").inputStream().use { inputStream ->
        properties.load(inputStream)
        publishingEnabled = properties.getProperty("publishingEnabled")?.toBoolean() ?: true
        token = properties.getProperty("token") ?: ""
        @Suppress("RemoveSingleExpressionStringTemplate")
        projectId = "${properties.getProperty("projectId")}".toInt()
    }
    return GitLabSettings(token = token, projectId = projectId, publishingEnabled = publishingEnabled)
}

data class ProjectSettings(val libVer: String, val isDevVer: Boolean)

fun fetchProjectSettings(): ProjectSettings {
    var libVer = "SNAPSHOT"
    var isDevVer = true
    val properties = Properties()
    file("project.properties").inputStream().use { inputStream ->
        properties.load(inputStream)
        libVer = properties.getProperty("libVer") ?: "SNAPSHOT"
        @Suppress("RemoveSingleExpressionStringTemplate")
        isDevVer = "${properties.getProperty("isDevVer")}".toBoolean()
    }
    return ProjectSettings(libVer = libVer, isDevVer = isDevVer)
}

val Boolean.intValue: Int
    get() = if (this) 1 else 0
